/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (sourceTopic) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.mapper;

import ch.quantasy.mdservice.message.annotations.AValidator;
import ch.quantasy.mdservice.message.annotations.NonNull;
import ch.quantasy.mdservice.message.annotations.Nullable;
import ch.quantasy.mdservice.message.annotations.StringForm;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author reto
 */
public class MapperSource extends AValidator implements Comparable<MapperSource> {

    @NonNull
    @StringForm
    public String id;

    @NonNull
    public Boolean enabled;

    @NonNull
    @StringForm()
    public String sourceTopic;

    @Nullable
    @StringForm
    public String description;

    @Nullable
    public SortedMap<String, MapperCondition> conditions;

    public MapperSource() {
        conditions = new TreeMap<>();
    }

    public MapperSource(String id, Boolean enabled, String topic) {
        this(id, enabled, topic, null);
    }

    public MapperSource(String id, Boolean enabled, String sourceTopic, String description) {
        this();
        this.id = id;
        this.enabled = enabled;
        this.sourceTopic = sourceTopic;
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MapperSource other = (MapperSource) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(MapperSource arg0) {
        return this.id.compareTo(arg0.id);
    }

}
