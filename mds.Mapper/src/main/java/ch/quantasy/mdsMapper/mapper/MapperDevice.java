/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (target) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.mapper;

import ch.quantasy.mdsMapper.gateway.binding.mapper.TargetConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperIntent;
import ch.quantasy.mdsMapper.gateway.binding.mapper.SourceConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.ConditionConfig;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author reto
 */
public class MapperDevice {

    private SortedMap<String, MapperTarget> targetMap;
    private MapperDeviceCallback mapperDeviceCallback;

    public MapperDevice() {
        targetMap = new TreeMap<>();
    }

    public MapperDevice(MapperDeviceCallback mapperDeviceCallback) {
        this();
        this.mapperDeviceCallback = mapperDeviceCallback;

    }

    public synchronized MapperTarget getMapperTarget(String targetId) {
        return targetMap.get(targetId);
    }

    public synchronized void update(MapperIntent intent) {
        if (intent == null || !intent.isValid()) {
            return;
        }
        if (intent.targetConfig != null) {
            manageTarget(intent.targetConfig);
        }
        if (intent.sourceConfig != null) {
            manageSource(intent.sourceConfig);
        }
        if (intent.conditionConfig != null) {
            manageSelector(intent.conditionConfig);
        }
    }

    public void manageTarget(Set<TargetConfig> targetConfig) {
        for (TargetConfig config : targetConfig) {
            switch (config.action) {
                case remove:
                    MapperTarget oldTarget = targetMap.remove(config.id);
                    if (oldTarget != null) {
                        mapperDeviceCallback.targetRemoved(oldTarget);
                    }
                    break;
                case put:
                    MapperTarget target = targetMap.get(config.id);
                    if (target == null) {
                        target = new MapperTarget(config.id, config.enabled, config.topic, config.intent, config.message, config.description);
                        if (!target.isValid()) {
                            break;
                        }
                        targetMap.put(target.id, target);
                        mapperDeviceCallback.targetInserted(target);
                    } else {
                        boolean updated = false;
                        if (config.description != null) {
                            target.description = config.description;
                            updated |= true;
                        }
                        if (config.topic != null) {
                            target.topic = config.topic;
                            updated |= true;

                        }
                        if (config.message != null) {
                            target.message = config.message;
                            updated |= true;
                        }
                        if (config.enabled != null) {
                            target.enabled = config.enabled;
                            updated |= true;

                        }
                        if (updated) {
                            mapperDeviceCallback.targetUpdated(target);
                        }
                    }
                    break;
            }
        }
    }

    public void manageSource(Set<SourceConfig> sourceConfig) {
        for (SourceConfig config : sourceConfig) {
            MapperTarget target = targetMap.get(config.targetId);
            if (target == null) {
                continue;
            }
            SortedMap<String, MapperSource> sourceMap = target.sources;
            switch (config.action) {
                case remove:
                    MapperSource oldOrigin = sourceMap.remove(config.id);
                    if (oldOrigin != null) {
                        mapperDeviceCallback.sourceRemoved(target, oldOrigin);
                    }
                    break;

                case put:
                    MapperSource source = sourceMap.get(config.id);
                    if (source == null) {
                        source = new MapperSource(config.id, config.enabled, config.topic, config.description);
                        if (!source.isValid()) {
                            break;
                        }
                        sourceMap.put(source.id, source);
                        mapperDeviceCallback.sourceInserted(target, source);
                    } else {
                        boolean updated = false;
                        if (config.description != null) {
                            source.description = config.description;
                            updated |= true;
                        }
                        if (config.topic != null) {
                            source.sourceTopic = config.topic;
                            updated |= true;

                        }
                        if (config.enabled != null) {
                            source.enabled = config.enabled;
                            updated |= true;

                        }
                        if (updated) {
                            mapperDeviceCallback.sourceUpdated(target, source);
                        }
                    }
                    break;
            }
        }
    }

    public void manageSelector(Set<ConditionConfig> selectorConfig) {
        for (ConditionConfig config : selectorConfig) {
            MapperTarget target = targetMap.get(config.targetId);
            if (target == null) {
                continue;
            }
            MapperSource source = target.sources.get(config.sourceId);
            if (source == null) {
                continue;
            }
            SortedMap<String, MapperCondition> selectorMap = source.conditions;
            switch (config.action) {
                case remove:
                    MapperCondition oldSelector = selectorMap.remove(config.id);
                    if (oldSelector != null) {
                        mapperDeviceCallback.conditionRemoved(target, source, oldSelector);
                    }
                    break;

                case put:
                    MapperCondition selector = selectorMap.get(config.id);
                    if (selector == null) {
                        selector = new MapperCondition(config.id, config.enabled, config.jsonPath, config.description);
                        if (!selector.isValid()) {
                            break;
                        }
                        selectorMap.put(selector.id, selector);
                        mapperDeviceCallback.conditionInserted(target, source, selector);
                    } else {
                        boolean updated = false;
                        if (config.description != null) {
                            selector.description = config.description;
                            updated |= true;
                        }
                        if (config.jsonPath != null) {
                            selector.jsonPath = config.jsonPath;
                            updated |= true;

                        }
                        if (config.enabled != null) {
                            selector.enabled = config.enabled;
                            updated |= true;
                        }
                        
                        mapperDeviceCallback.conditionUpdated(target, source, selector);
                    }
                    break;
            }

        }

    }
}
