/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (alias) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper;

import ch.quantasy.mdsMapper.gateway.binding.mapper.TargetConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperIntent;
import ch.quantasy.mdsMapper.gateway.binding.mapper.SourceConfig;
import ch.quantasy.mdsMapper.gateway.binding.mapper.ConditionConfig;
import ch.quantasy.mdsMapper.mapper.MapperCondition;
import ch.quantasy.mdservice.message.Message;
import ch.quantasy.mdsmqtt.gateway.client.contract.AyamlServiceContract;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import java.io.IOException;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import java.util.List;
import java.util.Map;

/**
 *
 * @author reto
 */
public class MapperExamplePrint {

    public static void main(String[] args) throws JsonProcessingException, IOException {
        AyamlServiceContract mapperContract = new AyamlServiceContract("Tester", "Test") {
            @Override
            public void setMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
            }
        };

        MapperIntent intent = new MapperIntent();
        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "I1", "TF/RemoteSwitch/U/jKQ/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOn\n  systemCode: D\n  deviceCode: 3\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "I1", "1", Boolean.TRUE, "Home/Fenster/EG/Wohnzimmer", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "I1", "1", "1", Boolean.TRUE, "$.message.[?(@['value']=='opened')]", "Fenster 3"));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "I1", "2", Boolean.TRUE, "TF/RemoteSwitch/U/jKQ/I", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "I1", "2", "1", Boolean.TRUE, "$.switchSocketCParameters.[?(@['switchingValue']!='switchOn')]", null));

        intent.targetConfig.add(new TargetConfig(TargetConfig.Action.put, "I2", "TF/RemoteSwitch/U/jKQ/I", Boolean.TRUE, Boolean.TRUE, mapperContract.getDocumentFor("switchSocketCParameters:\n  switchingValue: switchOff\n  systemCode: D\n  deviceCode: 3\n"), null));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "I2", "1", Boolean.TRUE, "Home/Fenster/EG/Wohnzimmer", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "I2", "1", "1", Boolean.TRUE, "$.message.[?(@['value']=='closed')]", "Fenster 3"));
        intent.sourceConfig.add(new SourceConfig(SourceConfig.Action.put, "I2", "2", Boolean.TRUE, "TF/RemoteSwitch/U/jKQ/I", null));
        intent.conditionConfig.add(new ConditionConfig(ConditionConfig.Action.put, "I2", "2", "1", Boolean.TRUE, "$.switchSocketCParameters.[?(@['switchingValue']!='switchOff')]", null));

        System.out.println(mapperContract.getObjectMapper().writeValueAsString(intent));

        String yamlString = "---\n"
                + "book:\n"
                + "- title: Beginning JSON\n"
                + "  author: Ben Smith\n"
                + "  price: 49.99\n"
                + "- title: JSON at Work\n"
                + "  author: Tom Marrs\n"
                + "  price: 29.99\n"
                + "- title: Learn JSON in a DAY\n"
                + "  author: Acodemy\n"
                + "  price: 8.99\n"
                + "- title: 'JSON: Questions and Answers'\n"
                + "  author: George Duckett\n"
                + "  price: 6\n"
                + "price range:\n"
                + "  cheap: 10\n"
                + "  medium: 20";
        JsonNode node = mapperContract.getDocumentFor(yamlString);
        System.out.println(node);
        ObjectMapper jsonWriter = new ObjectMapper();
        String jsonString = jsonWriter.writeValueAsString(node);
        
        //That would be cool... jayway and Jackson in one go... however, get an exception
        //Configuration configuration = Configuration.builder().jsonProvider(new JacksonJsonNodeJsonProvider(mapperContract.getObjectMapper()))
        //        .options(Option.SUPPRESS_EXCEPTIONS).build();
        //So patch:
        Configuration configuration = Configuration.defaultConfiguration();
        configuration.addOptions(Option.ALWAYS_RETURN_LIST);
        configuration.addOptions(Option.SUPPRESS_EXCEPTIONS);
        List<Map<String, Object>> expensive = JsonPath.parse(jsonString,configuration)
                .read("$['book'][?(@['price'] > $['price range']['medium'])]");
        expensive.forEach((arg0) -> {
            System.out.println(arg0);
        });
    }

}
