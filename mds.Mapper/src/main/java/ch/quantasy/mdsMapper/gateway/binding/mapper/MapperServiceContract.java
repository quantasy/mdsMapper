/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (aliasOrigin) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.gateway.binding.mapper;

import ch.quantasy.mdservice.message.Message;
import ch.quantasy.mdsmqtt.gateway.client.contract.AyamlServiceContract;
import java.util.Map;

/**
 *
 * @author reto
 */
public class MapperServiceContract extends AyamlServiceContract {

    private final String CONFIGURATION;
    public final String STATUS_CONFIGURATION;

    public MapperServiceContract(String instance) {
        super("Mapper", instance);
        CONFIGURATION = "configuration";
        STATUS_CONFIGURATION = STATUS + "/" + CONFIGURATION;

    }

    @Override
    public void setMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
        messageTopicMap.put(INTENT, MapperIntent.class);
        messageTopicMap.put("/<target>", MapperEvent.class);
//        messageTopicMap.put("<targetIntent>\n  required: # this tag is not part of the data structure\n    <message> #the content of target /message if target /intent: true \n", null);
        messageTopicMap.put(STATUS_CONFIGURATION + "/<target>/<ID>", MapperStatus.class);
        
    }
}
