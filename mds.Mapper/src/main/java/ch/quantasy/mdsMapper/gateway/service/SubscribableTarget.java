/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (target) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.gateway.service;

import ch.quantasy.mdsMapper.mapper.MapperTarget;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperEvent;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperServiceContract;
import ch.quantasy.mdsMapper.mapper.MapperSource;
import ch.quantasy.mdsMapper.gateway.binding.mapper.SourceMessage;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reto
 */
public class SubscribableTarget implements SubscribableSourceCallbackListener {

    private MQTTGatewayClient<MapperServiceContract> mqttClient;
    private MapperTarget target;
    private Map<String, SubscribableSource> subscribableSourceMap;

    public SubscribableTarget(MQTTGatewayClient mqttClient, MapperTarget target) {
        this.subscribableSourceMap = new HashMap<>();
        this.mqttClient = mqttClient;
        this.target = target;
        update();
    }

    public void update() {
        Set<String> sourceTopics = new HashSet<>();
        for (MapperSource source : target.sources.values()) {
            if (!subscribableSourceMap.containsKey(source.sourceTopic)) {
                SubscribableSource subscribableOrigin = new SubscribableSource(mqttClient.getContract(), source, this);
                subscribableSourceMap.put(source.sourceTopic, subscribableOrigin);
                mqttClient.subscribe(source.sourceTopic, subscribableOrigin);
            }
            sourceTopics.add(source.sourceTopic);
        }
        Set<String> removableTopics;
        removableTopics = new HashSet(subscribableSourceMap.keySet());
        removableTopics.removeAll(sourceTopics);
        for (String removableTopic : removableTopics) {
            remove(removableTopic);
        }
    }

    public void remove(String sourceTopic) {
        SubscribableSource subscribableOrigin = subscribableSourceMap.remove(sourceTopic);
        mqttClient.unsubscribe(sourceTopic, subscribableOrigin);
    }

    public void remove() {
        //TODO: Achtung: nicht erlaubt, wird eine ConcurrentModificationException
        for (String sourceTopic : subscribableSourceMap.keySet()) {
            remove(sourceTopic);
        }
    }

    @Override
    public void messageArrived(MapperSource source) {
        MapperEvent event = new MapperEvent(target.id, target.message);
        for (SubscribableSource subscribableSource : subscribableSourceMap.values()) {
            JsonNode sourceMessage = subscribableSource.getSourceMessage();
            if (sourceMessage == null) {
                return;
            }
            event.messages.add(new SourceMessage(source.id, source.sourceTopic, sourceMessage));
        }
        if (target.intent!=null && target.intent) {
            try {
                mqttClient.readyToPublish(target.topic, mqttClient.getContract().getObjectMapper().writeValueAsBytes(event.message));
            } catch (Exception ex) {
                Logger.getLogger(SubscribableTarget.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            mqttClient.readyToPublish(target.topic, event);
        }
    }

}
