/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (targetOrigin) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.gateway.service;

import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperStatus;
import ch.quantasy.mdsMapper.mapper.MapperTarget;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperIntent;
import ch.quantasy.mdsMapper.gateway.binding.mapper.Source;
import ch.quantasy.mdsMapper.gateway.binding.mapper.Condition;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperServiceContract;
import ch.quantasy.mdsMapper.mapper.MapperCondition;
import ch.quantasy.mdsMapper.mapper.MapperDevice;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import ch.quantasy.mdsMapper.mapper.MapperDeviceCallback;
import ch.quantasy.mdsMapper.mapper.MapperSource;

/**
 *
 * @author reto
 */
public class MapperService extends MQTTGatewayClient<MapperServiceContract> implements MapperDeviceCallback {

    private final MapperDevice device;
    private final Map<String, SubscribableTarget> subscribableTargetMap;

    public MapperService(URI mqttURI, String instanceName) throws MqttException {
        super(mqttURI, "MapperService:" + instanceName, new MapperServiceContract(instanceName));
        subscribableTargetMap = new HashMap<>();

        device = new MapperDevice(this);

        subscribe(getContract().INTENT + "/#", (topic, payload) -> {
            try {
                Set<MapperIntent> mapperIntents = toMessageSet(payload, MapperIntent.class);
                for (MapperIntent mapperIntent : mapperIntents) {
                    if (!mapperIntent.isValid()) {
                        continue;
                    }
                    device.update(mapperIntent);
                }
            } catch (Exception ex) {
                Logger.getLogger(MapperService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        );
        super.connect();
    }

    @Override
    public void targetRemoved(MapperTarget oldAlias) {
        SubscribableTarget subscribableAlias = subscribableTargetMap.remove(oldAlias.id);
        if (subscribableAlias != null) {
            subscribableAlias.remove();
            clearPublish(getContract().STATUS_CONFIGURATION + "/" + oldAlias.topic + "/" + oldAlias.id);
        }
    }

    @Override
    public void targetInserted(MapperTarget target) {
        subscribableTargetMap.put(target.id, new SubscribableTarget(this, target));
        publishTargetStatus(target);
    }

    @Override
    public void targetUpdated(MapperTarget target) {
        publishTargetStatus(target);
    }

    @Override
    public void sourceRemoved(MapperTarget target, MapperSource oldOrigin) {
        SubscribableTarget subscribableAlias = subscribableTargetMap.get(target.id);
        if (subscribableAlias != null) {
            subscribableAlias.remove(oldOrigin.sourceTopic);
            publishTargetStatus(target);
    
        }
    }

    @Override
    public void sourceInserted(MapperTarget target, MapperSource source) {
        SubscribableTarget subscribableAlias = subscribableTargetMap.get(target.id);
        if (subscribableAlias != null) {
            subscribableAlias.update();
            publishTargetStatus(target);

        }
    }

    @Override
    public void sourceUpdated(MapperTarget target, MapperSource source) {
        SubscribableTarget subscribableAlias = subscribableTargetMap.get(target.id);
        if (subscribableAlias != null) {
            subscribableAlias.update();
            publishTargetStatus(target);

        }
    }

    @Override
    public void conditionRemoved(MapperTarget target, MapperSource source, MapperCondition oldSelector) {
        //That does not matter, as the selector is read out at each messageArrived
        publishTargetStatus(target);

    }

    @Override
    public void conditionInserted(MapperTarget target, MapperSource source, MapperCondition selector) {
        //That does not matter, as the selector is read out at each messageArrived
        publishTargetStatus(target);

    }

    @Override
    public void conditionUpdated(MapperTarget target, MapperSource source, MapperCondition selector) {
        //That does not matter, as the selector is read out at each messageArrived
        publishTargetStatus(target);

    }

    private void publishTargetStatus(MapperTarget target) {
        super.getCollector().clearMessages(getContract().STATUS_CONFIGURATION + "/" + target.topic + "/" + target.id);
        MapperStatus status = new MapperStatus(target.id, target.topic, target.message, target.enabled, target.description);
        target.sources.forEach((arg0, source) -> {
            Source newSource = new Source(source.id, source.enabled, source.sourceTopic, source.description);
            status.sources.add(newSource);
            source.conditions.forEach((arg1, selector) -> {
                newSource.conditions.add(new Condition(selector.id, selector.enabled, selector.jsonPath, selector.description));
            });
        });
        readyToPublish(getContract().STATUS_CONFIGURATION + "/" + target.topic + "/" + target.id, status);

    }

}
