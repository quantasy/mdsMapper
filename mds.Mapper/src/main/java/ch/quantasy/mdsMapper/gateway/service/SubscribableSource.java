/*
 *   "mdsMapper"
 *
 *    mdsMapper(tm): A gateway to provide universal mapping (alias) ability.
 *
 *    Copyright (c) 2019 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsMapper.gateway.service;

import ch.quantasy.mdsMapper.mapper.MapperCondition;
import ch.quantasy.mdsMapper.gateway.binding.mapper.MapperServiceContract;
import ch.quantasy.mdsMapper.mapper.MapperSource;
import ch.quantasy.mdservice.message.MessageReceiver;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author reto
 */
public class SubscribableSource implements MessageReceiver {

    private MapperSource origin;
    private MapperServiceContract contract;
    private JsonNode originMessage;
    private SubscribableSourceCallbackListener listener;

    public SubscribableSource(MapperServiceContract contract, MapperSource origin, SubscribableSourceCallbackListener listener) {
        this.origin = origin;
        this.contract = contract;
        this.listener = listener;
    }

    @Override
    public void messageReceived(String topic, byte[] payload) throws Exception {
        boolean hit = true;
        JsonNode document = contract.getDocumentFor(payload); //Accepts YAML and json

        if (!origin.conditions.isEmpty()) {

            //Maybe, we have an array of events... maybe not so, we have to figure out and act accordingly
            List<JsonNode> elements = new ArrayList<>();
            if (document.isArray()) {
                for (JsonNode element : document) {
                    elements.add(element);
                }
            } else {
                elements.add(document);
            }
            boolean queryHit = false;
            for (MapperCondition selector : origin.conditions.values()) {
                for (JsonNode element : elements) {
//                    //JsonNode node = element.at(selector.jsonPath);
//                    //if (node.isMissingNode()) {
//                    //    continue;
//                    //}
//                    String jsonString = jsonMapper.writeValueAsString(element);
//                    //That would be cool... jayway and Jackson in one go... however, get an exception
//                    //Configuration configuration = Configuration.builder().jsonProvider(new JacksonJsonNodeJsonProvider(mapperContract.getObjectMapper()))
//                    //        .options(Option.SUPPRESS_EXCEPTIONS).build();
//                    //So patch:

                    try {
                        ArrayNode result = contract.getResultForQuery(element, selector.jsonPath);
                        queryHit = result.size() > 0;
                    } catch (Exception ex) {
                        //Some exception...
                        
                        queryHit = false;
                    }
                    //If we find the value in one of the events, we are done, hit is true //OR
                    if (queryHit) {
                        break;
                    }
                }
                hit &= queryHit;
                //If we find one instance, where queryHit is false, we are done, hit is false  //AND
                if (!hit) {
                    break;
                }
            }

        }
        if (hit) {
            originMessage = document;
            listener.messageArrived(origin);
        } else {
            originMessage = null;
        }
    }

    public JsonNode getSourceMessage() {
        return originMessage;
    }

    public static void main(String[] args) {
        MapperServiceContract contract = new MapperServiceContract("me");
        String s = "{Person:"
                + "[\n"
                + "   {\n"
                + "      \"name\" : \"john\",\n"
                + "      \"gender\" : \"male\"\n"
                + "   },\n"
                + "   {\n"
                + "      \"name\" : \"ben\",\n"
                + "      \"gender\" : \"female\"\n"
                + "   }\n"
                + "]}";

        //Works fine
        ArrayNode genders0 = contract.getResultForQuery(s,"$.Person..['gender']");
//PathNotFoundException thrown
        ArrayNode genders1 = contract.getResultForQuery(s,"$.Person.[1]['gender']");
        ArrayNode result = contract.getResultForQuery(s,"$.Person.[1].gnender");
        System.out.println(result.size());
    }
}
