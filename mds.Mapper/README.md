# mdsMapper
[Data-driven] [micro-service] for Mapping source topics to aliases.



## API

                                                                        
### <id>
```
/<target>
   required: # this tag is not part of the data structure
     messages: Set <min: 0 max: 9223372036854775807>
     messages:
      required: # this tag is not part of the data structure
        sourceId: String <regEx: [\S\s]*>
        topic: String <regEx: [\S\s]*>
        value: Document <type: yaml>
     targetId: String <regEx: [\S\s]*>
     timeStamp: Number <from: 0 to: 9223372036854775807> # default: Current time in nano seconds
   optional: # this tag is not part of the data structure
     message: Document <type: yaml>
   
```
```
<targetIntent>
  required: # this tag is not part of the data structure
    <message> #the content of target /message if target /intent: true 

   
```
```
Mapper/<id>/I
   required: # this tag is not part of the data structure
     timeStamp: Number <from: 0 to: 9223372036854775807> # default: Current time in nano seconds
   optional: # this tag is not part of the data structure
     conditionConfig: Set <min: 0 max: 9223372036854775807>
     conditionConfig:
      required: # this tag is not part of the data structure
        action: String <put,remove>
        id: String <regEx: [\S\s]*>
        sourceId: String <regEx: [\S\s]*>
        targetId: String <regEx: [\S\s]*>
      optional: # this tag is not part of the data structure
        description: String <regEx: [\S\s]*>
        enabled: Boolean <true,false>
        jsonPath: String <regEx: [\S\s]*>
     sourceConfig: Set <min: 0 max: 9223372036854775807>
     sourceConfig:
      required: # this tag is not part of the data structure
        action: String <put,remove>
        id: String <regEx: [\S\s]*>
        targetId: String <regEx: [\S\s]*>
      optional: # this tag is not part of the data structure
        description: String <regEx: [\S\s]*>
        enabled: Boolean <true,false>
        topic: String <regEx: [\S\s]*>
     targetConfig: Set <min: 0 max: 9223372036854775807>
     targetConfig:
      required: # this tag is not part of the data structure
        action: String <put,remove>
        id: String <regEx: [\S\s]*>
      optional: # this tag is not part of the data structure
        description: String <regEx: [\S\s]*>
        enabled: Boolean <true,false>
        intent: Boolean <true,false>
        message: Document <type: yaml>
        topic: String <regEx: [\\S\\s]*[^\+\#]*>
   
```
```
Mapper/<id>/S/configuration/<target>/<ID>
   required: # this tag is not part of the data structure
     id: String <regEx: [\S\s]*>
     timeStamp: Number <from: 0 to: 9223372036854775807> # default: Current time in nano seconds
   optional: # this tag is not part of the data structure
     description: String <regEx: [\S\s]*>
     enabled: Boolean <true,false>
     message: Document <type: yaml>
     sources: Set <min: 0 max: 9223372036854775807>
     sources:
      required: # this tag is not part of the data structure
        id: String <regEx: [\S\s]*>
      optional: # this tag is not part of the data structure
        conditions: Set <min: 0 max: 9223372036854775807>
        conditions:
         required: # this tag is not part of the data structure
           id: String <regEx: [\S\s]*>
         optional: # this tag is not part of the data structure
           description: String <regEx: [\S\s]*>
           enabled: Boolean <true,false>
           jsonPath: String <regEx: [\S\s]*>
        description: String <regEx: [\S\s]*>
        enabled: Boolean <true,false>
        topic: String <regEx: [\S\s]*>
     topic: String <regEx: [\\S\\s]*[^\+\#]*>
   
```
```
Mapper/<id>/S/connection
   required: # this tag is not part of the data structure
     timeStamp: Number <from: 0 to: 9223372036854775807> # default: Current time in nano seconds
     value: String <[online, offline]>
   
```